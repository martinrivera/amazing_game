{
    "id": "f6d72534-1327-45d2-b9c8-84d1fea74229",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "addf563c-e4d5-40bf-a557-ac442529dc44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "50034897-1354-4b60-8da7-0d91f9cdf80c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "addf563c-e4d5-40bf-a557-ac442529dc44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da67a53d-f10b-451f-afdb-b1f6eb079d34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "addf563c-e4d5-40bf-a557-ac442529dc44",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        },
        {
            "id": "585aa973-03ec-4481-b16b-0a2da5914651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "73fb1b65-eca5-4ca0-b548-a0176de45277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "585aa973-03ec-4481-b16b-0a2da5914651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14431f9f-726b-46ab-991c-1b1d302073fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "585aa973-03ec-4481-b16b-0a2da5914651",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        },
        {
            "id": "c4665562-7895-4891-8f2b-fbed5b21bb70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "c5180663-6a76-43e9-94e8-e701612cd6a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4665562-7895-4891-8f2b-fbed5b21bb70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "737c80df-f291-4ad4-b09a-34689e1c475b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4665562-7895-4891-8f2b-fbed5b21bb70",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        },
        {
            "id": "dfef5bcb-1b15-450e-bb14-1056146ce025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "26cc3959-e308-4a97-a064-18ecf886c5a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfef5bcb-1b15-450e-bb14-1056146ce025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10d83e73-fa4c-4f3b-af7a-847055f66b8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfef5bcb-1b15-450e-bb14-1056146ce025",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        },
        {
            "id": "f0e4d6c5-9d64-4637-b540-eb3ce11a23e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "e8e8cfd2-20e7-4968-ac3d-83ea514b5096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e4d6c5-9d64-4637-b540-eb3ce11a23e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d099aa2a-a7b7-42cb-a2e8-c2bd054764c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e4d6c5-9d64-4637-b540-eb3ce11a23e1",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        },
        {
            "id": "e85c9cf0-ecca-47cb-b4db-fab3daa07557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "51691d72-cdda-4c7b-9a67-6110503a81ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e85c9cf0-ecca-47cb-b4db-fab3daa07557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb570e37-f28f-4301-a910-f62cec3e0091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e85c9cf0-ecca-47cb-b4db-fab3daa07557",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        },
        {
            "id": "bc1cc735-ef10-4a55-8c29-b120be6c6ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "e5712e53-dc21-443b-ac2d-668ac9446ae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1cc735-ef10-4a55-8c29-b120be6c6ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "190a8783-3f0a-45b1-b674-21ff34dffcb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1cc735-ef10-4a55-8c29-b120be6c6ec1",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        },
        {
            "id": "8790fa2d-d1c6-4f50-80bb-b500872ce831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "compositeImage": {
                "id": "6794eb8f-864e-41df-814b-46b47b61a7d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8790fa2d-d1c6-4f50-80bb-b500872ce831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39aceaa7-6d7d-4e86-a7a0-7c4bcf3f455e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8790fa2d-d1c6-4f50-80bb-b500872ce831",
                    "LayerId": "9f91badc-130c-4309-be0a-4137224c43db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9f91badc-130c-4309-be0a-4137224c43db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6d72534-1327-45d2-b9c8-84d1fea74229",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}